FROM openjdk:11.0.10-jdk-slim
VOLUME /tmp
ADD /target/agendamento-0.0.1-SNAPSHOT.jar app.jar
ADD ./key.json gcloud-key.json
ENV GOOGLE_APPLICATION_CREDENTIALS /gcloud-key.json
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
