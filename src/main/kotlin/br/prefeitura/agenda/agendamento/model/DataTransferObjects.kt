package br.prefeitura.agenda.agendamento.model

import java.time.LocalDateTime

data class NovoAgendamentoDTO(
    val idPaciente: Long,
    val idUnidade: Long,
    val idProfissional: Long,
    val dataAgendamento: LocalDateTime
)

data class AgendamentoSalvoDTO(val idAgendamento: Long)