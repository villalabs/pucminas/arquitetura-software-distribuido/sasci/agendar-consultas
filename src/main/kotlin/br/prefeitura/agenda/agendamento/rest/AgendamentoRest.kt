package br.prefeitura.agenda.agendamento.rest

import br.prefeitura.agenda.agendamento.model.AgendamentoSalvoDTO
import br.prefeitura.agenda.agendamento.model.NovoAgendamentoDTO
import br.prefeitura.agenda.agendamento.service.AgendamentoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/agendamento")
class AgendamentoRest {

    @Autowired lateinit var agendamentoService: AgendamentoService

    @PostMapping
    fun getAgendamentos(@RequestBody agendamento: NovoAgendamentoDTO): AgendamentoSalvoDTO? {
        return agendamentoService.agendarConsulta(agendamento)
    }
}
