package br.prefeitura.agenda.agendamento.repository

import br.prefeitura.agenda.agendamento.model.Agendamento
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AgendamentoRepository : JpaRepository<Agendamento, Long> {
}