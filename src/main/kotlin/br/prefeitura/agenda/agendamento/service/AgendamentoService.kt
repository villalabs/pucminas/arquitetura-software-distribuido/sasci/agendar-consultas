package br.prefeitura.agenda.agendamento.service

import br.prefeitura.agenda.agendamento.model.*
import br.prefeitura.agenda.agendamento.repository.AgendamentoRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AgendamentoService {

    @Autowired
    lateinit var agendamentoRepository: AgendamentoRepository

    val logger = LoggerFactory.getLogger(AgendamentoService::class.java)

    fun agendarConsulta(agendamento: NovoAgendamentoDTO): AgendamentoSalvoDTO? {
        val novoAgendamento = Agendamento(
            data = agendamento.dataAgendamento,
            paciente = Paciente(agendamento.idPaciente),
            unidade = Unidade(agendamento.idUnidade),
            profissional = Profissional(agendamento.idProfissional)
        )
        return agendamentoRepository.saveAndFlush(novoAgendamento).let { AgendamentoSalvoDTO(it.id!!) }
    }
}
